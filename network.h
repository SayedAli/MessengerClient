#ifndef NETWORK_H
#define NETWORK_H

#include<QNetworkAccessManager>
#include<QNetworkRequest>
#include<QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>


class Network:public QObject
{
public:
    Network();
    QJsonObject sendRequest(QString);
    ~Network();
private:
    QNetworkAccessManager *manager;
    QNetworkRequest request;
    QNetworkReply *reply;
};

#endif // NETWORK_H
