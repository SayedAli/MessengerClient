#include "entrance.h"
#include "ui_entrance.h"



QNetworkAccessManager* manager;

QNetworkRequest req;


QString serverAddress = "http://api.softserver.org:1104/" , sendAddress;

QString passWord , userName , firstName , lastName;
QString passwordHided , token;

QFile lastLoginFile("lastLogin.json");


Entrance::Entrance(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Entrance)
{
    ui->setupUi(this);
    manager = new QNetworkAccessManager(this);

//    networkManager = new Network;
}

Entrance::~Entrance()
{
    delete ui;
    delete manager;
//    delete networkManager;

}

bool Entrance::checkInternetConnection()
{
    QNetworkAccessManager sample;
    QNetworkRequest req(QUrl("http://www.google.com"));
    QNetworkReply *reply = sample.get(req);
    QEventLoop loop;
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));

    loop.exec();
    disconnect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    if(reply->bytesAvailable())
        return true;
    else
        return false;
}

void Entrance::registerReplyFinished(QNetworkReply *reply){
    ui->label_7->clear();
    QJsonDocument jsdoc;
    jsdoc = QJsonDocument::fromJson(reply->readAll());
    mainObject = jsdoc.object();

    ui->label_7->setText(mainObject["message"].toString());

}

void Entrance::loginReplyFinished(QNetworkReply *reply){
    ui->label_7->clear();
    QJsonDocument jsdoc;

    jsdoc = QJsonDocument::fromJson(reply->readAll());
    mainObject = jsdoc.object();

    token.push_back(mainObject["token"].toString());

    if(mainObject["message"].toString() == "Logged in Successfully"){


        if(!lastLoginFile.open(QIODevice::WriteOnly)){
          ui->label->clear();
          ui->label->setText("Error in opening lastLogin.json");
        }
        QVariantMap map;
        map.insert("username",userName);
        map.insert("password",passWord);
        map.insert("token",token);

        QJsonObject object = QJsonObject::fromVariantMap(map);

        QJsonDocument jsdoc;
        jsdoc.setObject(object);


        QTextStream out(&lastLoginFile);

        out << jsdoc.toJson() << flush;



        lastLoginFile.close();
        this->close();
        ChatWindow *window = new ChatWindow;
        window->showNormal();




    }
    else if(mainObject["message"].toString() == "You are already logged in!"){
        req.setUrl(QUrl(serverAddress +"logout?username="+userName + "&password="+passWord));
        req.setHeader(QNetworkRequest::ContentTypeHeader , "application/json");

        disconnect(manager , &QNetworkAccessManager::finished , this , &Entrance::loginReplyFinished);

        connect(manager , &QNetworkAccessManager::finished , this , &Entrance::logoutReplyFinished);

        manager->get(req);


    }
    else{
        ui->label_7->setText(mainObject["message"].toString());
    }



}

void Entrance::logoutReplyFinished(QNetworkReply *reply){
    QJsonDocument jsdoc;

    jsdoc = QJsonDocument::fromJson(reply->readAll());
    mainObject = jsdoc.object();

    if(mainObject["message"].toString() == "Logged Out Successfully"){
        req.setUrl(QUrl(serverAddress + "login?username=" + userName + "&password=" + passWord));
        req.setHeader(QNetworkRequest::ContentTypeHeader , "application/json");

        disconnect(manager , &QNetworkAccessManager::finished , this , &Entrance::logoutReplyFinished);

        connect(manager , &QNetworkAccessManager::finished , this , &Entrance::loginReplyFinished);

        manager->get(req);
    }
    else{
        ui->label_7->clear();
        ui->label_7->setText(mainObject["message"].toString());
    }
}


void Entrance::on_pushButton_clicked()
{
    if(Entrance::checkInternetConnection() == false){
        ui->label_7->setText("You are not connected to the Internet");
        return;
    }
    userName.clear();
    passWord.clear();
    firstName.clear();
    lastName.clear();


    userName.push_back(ui->lineEdit_4->text());
    passWord.push_back(ui->lineEdit_3->text());
    if(ui->lineEdit_2->text().size() != 0){

        firstName = ui->lineEdit_2->text();
    }
    if(ui->lineEdit->text().size() != 0){
       lastName = ui->lineEdit->text();
    }

    sendAddress.clear();
    sendAddress = serverAddress;
    if((firstName.size() == 0) && (lastName.size() == 0))
       sendAddress.append("signup?username=" + userName + "&password=" + passWord);
    else if((firstName.size() != 0) && (lastName.size() == 0))
       sendAddress.append("signup?username=" + userName + "&password=" + passWord + "&firstname=" + firstName);
    else if((firstName.size() == 0) && (lastName.size() != 0))
       sendAddress.append("signup?username=" + userName + "&password=" + passWord + "&lastname=" + lastName);
    else if((firstName.size() != 0) && (lastName.size() != 0))
       sendAddress.append("signup?username=" + userName + "&password=" + passWord + "&firstname=" + firstName + "&lastname=" + lastName);


    req.setUrl(QUrl(sendAddress));
    req.setHeader(QNetworkRequest::ContentTypeHeader , "application/json");

    disconnect(manager , &QNetworkAccessManager::finished , this , &Entrance::registerReplyFinished);
    disconnect(manager , &QNetworkAccessManager::finished , this , &Entrance::loginReplyFinished);

    connect(manager , &QNetworkAccessManager::finished , this , &Entrance::registerReplyFinished);

    manager->get(req);


//    QJsonObject mainObject =  networkManager->sendRequest(sendAddress);

//    qDebug()<<mainObject["message"].toString();
//    ui->label_7->setText(mainObject["message"].toString());

}

void Entrance::on_pushButton_2_clicked()
{

    ui->label_7->clear();
    if(Entrance::checkInternetConnection() == false){
        ui->label_7->setText("You are not connected to the Internet");
        return;
    }
    userName.clear();
    passWord.clear();
    userName.push_back(ui->lineEdit_5->text());
    passWord.push_back(ui->lineEdit_6->text());

    sendAddress.clear();
    sendAddress.push_back(serverAddress);

    sendAddress.append("login?username=" + userName + "&password=" + passWord);

//    networkManager->sendRequest(sendAddress);
//    QJsonObject mainObject = networkManager->getJsonObject();
    req.setUrl(QUrl(sendAddress));
    req.setHeader(QNetworkRequest::ContentTypeHeader , "application/json");

    disconnect(manager , &QNetworkAccessManager::finished , this , &Entrance::loginReplyFinished);
    disconnect(manager , &QNetworkAccessManager::finished , this , &Entrance::registerReplyFinished);
    connect(manager , &QNetworkAccessManager::finished , this , &Entrance::loginReplyFinished);

    manager->get(req);
//qDebug()<<"before m";
//qDebug()<<mainObject["message"].toString();
//    ui->label_7->setText(mainObject["message"].toString());
//    token.clear();
//    token.push_back(mainObject["token"].toString());

//    if(mainObject["message"].toString() == "Logged in Successfully"){
//         this->close();
//         ChatWindow *window = new ChatWindow;
//         window->show();
//    }
//    QJsonObject emptyJson;
//    mainObject = emptyJson;


}
