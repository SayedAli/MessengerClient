#ifndef ENTRANCE_H
#define ENTRANCE_H

#include <QMainWindow>
#include <QJsonDocument>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QDebug>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>

#include "chatwindow.h"
#include "ui_chatwindow.h"

//#include "network.h"

namespace Ui {
class Entrance;
}

class Entrance : public QMainWindow
{
    Q_OBJECT

public:
    explicit Entrance(QWidget *parent = 0);
    ~Entrance();
    bool checkInternetConnection();
    QJsonObject mainObject;

private slots:

    void on_pushButton_clicked();


    void registerReplyFinished(QNetworkReply *reply);

    void loginReplyFinished(QNetworkReply *reply);

    void logoutReplyFinished(QNetworkReply *reply);

    void on_pushButton_2_clicked();


private:
    Ui::Entrance *ui;
//    Network *networkManager;
};

#endif // ENTRANCE_H
