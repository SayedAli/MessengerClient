#-------------------------------------------------
#
# Project created by QtCreator 2018-06-01T11:28:31
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MessengerClient
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS


SOURCES += main.cpp\
            entrance.cpp \
             chatwindow.cpp \
               network.cpp

HEADERS  += entrance.h \
             chatwindow.h \
               network.h

FORMS    += entrance.ui \
             chatwindow.ui





