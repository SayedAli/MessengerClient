#include "entrance.h"
#include <QApplication>
#include "chatwindow.h"

//extern QNetworkAccessManager *manager;

//extern QNetworkRequest req;


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QFile lastLoginFile("lastLogin.json");

    if(!lastLoginFile.open(QIODevice::ReadOnly)){
        lastLoginFile.close();
        Entrance w;
        w.showNormal();

        return a.exec();
    }
   else{
        lastLoginFile.close();
        ChatWindow window;
        window.showNormal();
        return a.exec();




    }
}
