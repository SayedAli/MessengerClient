#include "network.h"
#include "entrance.h"
#include <QDebug>
#include <QThread>

Network::Network()
{
   manager = new QNetworkAccessManager(this);

}

Network::~Network(){
    delete manager;
}

QJsonObject Network::sendRequest(QString sendAddress){

    request.setUrl(QUrl(sendAddress));
    request.setHeader(QNetworkRequest::ContentTypeHeader , "application/json");

    reply = manager->get(request);
    while(!reply->isFinished()){
       QCoreApplication::processEvents();
    }
    QJsonDocument jsdoc = QJsonDocument::fromJson(reply->readAll());

    return (jsdoc.object());
}

