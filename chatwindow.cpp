#include "chatwindow.h"
#include "ui_chatwindow.h"


QString message;

extern QNetworkAccessManager *manager;

extern QNetworkRequest req;

extern QFile lastLoginFile;

extern QString token , userName , passWord , firstName , lastName;

QFile chatFile;

int i = 0;

ChatWindow::ChatWindow(QWidget *parent) :
   QMainWindow(parent),
    ui(new Ui::ChatWindow)
{
    ui->setupUi(this);

    if(lastLoginFile.isOpen())
       lastLoginFile.close();
    if(lastLoginFile.open(QIODevice::ReadOnly)){
        QJsonDocument jsdoc = QJsonDocument::fromJson(lastLoginFile.readAll());
        QJsonObject jsObject = jsdoc.object();

        userName = jsObject["username"].toString();
        passWord = jsObject["password"].toString();
        token = jsObject["token"].toString();
    }


    manager = new QNetworkAccessManager(this);
}

ChatWindow::~ChatWindow()
{
    delete ui;
    delete manager;
//    delete networkManager;
}

void ChatWindow::checkUserExistenceReplyFinished(QNetworkReply *reply){
     ui->label->clear();
     QJsonDocument jsdoc = QJsonDocument::fromJson(reply->readAll());
     QJsonObject mainObject = jsdoc.object();

     if(mainObject["code"].toString() != "404"){

//             networkManager->sendRequest("http://api.softserver.org:1104/logout?username=" + ui->lineEdit->text() + "&password" );
         for(int i = 0;i < ui->listWidget->count();i++){
             if(ui->listWidget->item(i)->text() == ui->lineEdit->text()){

                ui->label->setText("This User has been already added to your list");
                return;
             }
         }





          ui->label->setText("Added Successfully");
        ui->listWidget->addItem(ui->lineEdit->text());

      }
       else{

           ui->label->setText(mainObject["message"].toString());
         }


}

void ChatWindow::logoutReplyFinished(QNetworkReply *reply){
    QJsonDocument jsdoc = QJsonDocument::fromJson(reply->readAll());
    QJsonObject mainObject = jsdoc.object();
    if(mainObject["message"].toString() == "Logged Out Successfully"){

        token.clear();

       lastLoginFile.remove();
       lastLoginFile.close();
       this->close();
        Entrance *enterPage = new Entrance;
        enterPage->showNormal();
    }
    else{
      ui->label_2->clear();
      ui->label_2->setText(mainObject["message"].toString());
    }
}

void ChatWindow::on_pushButton_2_clicked()
{



}

void ChatWindow::on_pushButton_clicked()
{
    message.clear();
    message.push_back(ui->textEdit->toPlainText());

    ui->textEdit->clear();

    QVariantMap map;

    map.insert("username",userName);
    map.insert("message",message);
    QJsonObject jsMapObject = QJsonObject::fromVariantMap(map);

    if(!chatFile.open(QIODevice::ReadWrite)){
        ui->label_2->clear();
        ui->label_2->setText("Unable to open " + chatFile.fileName());
    }
    QJsonDocument jsDoc = QJsonDocument::fromJson(chatFile.readAll());
    QJsonObject jsObject = jsDoc.object();
    jsObject.insert(QString::number(jsObject.size()),jsMapObject);

    jsDoc.setObject(jsObject);

    chatFile.reset();

    QTextStream out(&chatFile);


    out << jsDoc.toJson() << flush;

    chatFile.close();

     ui->textBrowser->clear();

      for(int i = 0;i < jsObject.size();i++){

           QJsonObject jsMessageObj = jsObject[QString::number(i)].toObject();

              ui->textBrowser->insertPlainText(jsMessageObj["username"].toString() + " : " + jsMessageObj["message"].toString() + "\n");


      }

    chatFile.close();
}




void ChatWindow::on_pushButton_4_clicked()
{
//    QListWidgetItem item;
//    item.setText(ui->lineEdit->text());

//    QIcon icon;
//    icon.addFile("blue_user.png");
//    item.setIcon(icon);

    req.setUrl(QUrl("http://api.softserver.org:1104/logout?username=" + ui->lineEdit->text() +"&password"));
    req.setHeader(QNetworkRequest::ContentTypeHeader , "application/json");

    disconnect(manager , &QNetworkAccessManager::finished , this , &ChatWindow::logoutReplyFinished);
    disconnect(manager , &QNetworkAccessManager::finished , this , &ChatWindow::checkUserExistenceReplyFinished);

    connect(manager , &QNetworkAccessManager::finished , this , &ChatWindow::checkUserExistenceReplyFinished);

    manager->get(req);

//    networkManager->sendRequest("http://api.softserver.org:1104/login?username=" + ui->lineEdit->text() +"&password");
//    QJsonObject mainObject = networkManager->getJsonObject();


}



void ChatWindow::on_pushButton_7_clicked()
{

}

void ChatWindow::on_pushButton_8_clicked()
{

}

void ChatWindow::on_pushButton_3_clicked()
{
    //logoutButton



    req.setUrl(QUrl("http://api.softserver.org:1104/logout?username=" + userName +"&password=" + passWord));
    req.setHeader(QNetworkRequest::ContentTypeHeader , "application/json");

    disconnect(manager , &QNetworkAccessManager::finished , this , &ChatWindow::logoutReplyFinished);
    disconnect(manager , &QNetworkAccessManager::finished , this , &ChatWindow::checkUserExistenceReplyFinished);


    connect(manager , &QNetworkAccessManager::finished , this , &ChatWindow::logoutReplyFinished);

    manager->get(req);

}

void ChatWindow::on_listWidget_itemClicked(QListWidgetItem *item)
{
    chatFile.setFileName(item->text() + ".json");


    if(!chatFile.open(QIODevice::ReadWrite)){
        ui->label_2->clear();
        ui->label_2->setText("Unable to open " + item->text() + ".json" );
    }
    QJsonDocument jsDoc = QJsonDocument::fromJson(chatFile.readAll());

    QJsonObject jsObject = jsDoc.object();

    ui->textBrowser->clear();

    for(int i = 0;i < jsObject.size();i++){

         QJsonObject jsMessageObj = jsObject[QString::number(i)].toObject();

         ui->textBrowser->insertPlainText(jsMessageObj["username"].toString() + " : " + jsMessageObj["message"].toString() + "\n");


    }
    chatFile.close();



}
