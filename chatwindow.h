#ifndef CHATWINDOW_H
#define CHATWINDOW_H

#include "entrance.h"
#include "network.h"
#include <QMainWindow>
#include<QNetworkAccessManager>
#include<QNetworkRequest>
#include<QNetworkReply>
#include <QJsonDocument>
#include<QJsonObject>
#include <QFile>
#include <QListWidgetItem>


namespace Ui {
class ChatWindow;
}

class ChatWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit ChatWindow(QWidget *parent = 0);
    ~ChatWindow();


private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

    void checkUserExistenceReplyFinished(QNetworkReply *);

    void logoutReplyFinished(QNetworkReply *);

    void on_pushButton_4_clicked();

    void on_pushButton_7_clicked();

    void on_pushButton_8_clicked();

    void on_pushButton_3_clicked();

    void on_listWidget_itemClicked(QListWidgetItem *item);

private:
    Ui::ChatWindow *ui;
//    Network *networkManager;
};

#endif // CHATWINDOW_H
